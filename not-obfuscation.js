// What's this obfuscation sorcery?
// Nope. It's not obfuscation, OK!?

function printLines() {
    return arguments;
}

var message = String("Hello World This Is The Power Of JavaScript")
            . split(" ");

for (var word of printLines(message)[0])
    console.log(word);
